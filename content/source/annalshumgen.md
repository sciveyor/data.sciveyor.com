---
title: Annals of Human Genetics
---

# Annals of Human Genetics

This is a canonical data source description,
`https://data.sciveyor.com/source/annalshumgen`.  
The current `dataSourceVersion` described by this documentation is 1. The
`dataSource` name for this data is `Annals of Human Genetics`.

**Coverage:** _Annals of Eugenics_ and _Annals of Human Genetics,_ from first publication (October 1925) to August 1, 2022  
**Size:** 4,009 articles  
**Copyright:** Copyright Wiley Journals  
**License:** [Wiley Journals TDM License](http://doi.wiley.com/10.1002/tdm_license_1.1)  
**Credits:** C.H. Pence and Nicola Bertoldi

## How we got it

PDFs for these articles were downloaded directly from Wiley, via querying their
[text and data-mining
API.](https://onlinelibrary.wiley.com/library-info/resources/text-and-datamining)

## Processing

- **Bibliographic Information:** Crossref
- **PMIDs, PMCIDs, and PubMed Manuscript IDs:**
  [PubMed scraping]({{< relref "../technical-details/pubmed-scraping.md" >}})
- **Full text:**
  - For _Annals of Eugenics_: the disclaimer placed at the front of every
    article was removed, and [OCR was re-run]({{< relref
    "../technical-details/ocr.md" >}})
  - For _Annals of Human Genetics_: text was extracted directly from PDFs
- **Keywords and Tags:** "Subject" tags saved in Crossref have been preserved as
  tags.

## Changelog

- **Data Source Version 1 (2021-08-29):** initial content import.
