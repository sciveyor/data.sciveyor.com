---
title: Insecta Mundi
---

# Insecta Mundi

This is a canonical data source description,
`https://data.sciveyor.com/source/insectamundi`.

The current `dataSourceVersion` described by this documentation is 1. The
`dataSource` name for this data is `Insecta Mundi`.

**Coverage:** _Insecta Mundi,_ from its founding until June 2, 2021  
**Size:** 1,367 articles  
**Copyright:** CC-BY-NC 3.0  
**Credits:** Stijn Conix, C.H. Pence

## How we got it

This data was downloaded from the mirror of the journal hosted at the [Florida
Virtual Campus.](https://journals.flvc.org/mundi)

## Processing

- **Plain Text:**
  - "Old Series" (articles with volume and issue number, 1985–2006): [OCR was
    re-run]({{< relref "../technical-details/ocr.md" >}}) from each PDF file
  - "New Series" (articles with article number only, 2007-): text extracted from
    PDF files, initial two "title page" pages removed from each article
- **Metadata:** These articles are not registered with CrossRef and have no
  DOIs; minimal metadata was extracted from the FLVC pages for each article.

## Changelog

- **Data Source Version 1 (2022-09-07):** First import of IM data.
