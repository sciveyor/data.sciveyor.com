---
title: JSTOR
---

# JSTOR

This is a canonical data source description,
`https://data.sciveyor.com/source/jstor`.

The current `dataSourceVersion` described by this documentation is 1. The
`dataSource` name for this data is `JSTOR`.

**Coverage:** see list of journals below; coverage usually through around
mid-2018, when data was delivered, though this can change for individual
journals as a result of embargoes and moving walls  
**Size:** 1,265,573 articles  
**Copyright:** varies per article; see licensing fields  
**License:** an agreement between the Sciveyor project and
[JSTOR DFR](https://www.jstor.org/dfr), now a paid project called "Constellate"  
**Credits:** C.H. Pence, Grant Ramsey

## How we got it

This data was delivered to us directly by JSTOR, under the terms of our
licensing agreement. We cannot extend our coverage without a further agreement.

## Journals included

The following journals are available in this dataset. For each journal, you'll
find a title and an approximate number of articles available. Only journals with
more than 100 articles available are listed here; searches may find occasional
articles from rarer journals. Note also that if you are attempting to collect
all of a journal's print run into a single dataset, you should search for
alternative and variant titles, as some journals have changed title over time.

{{<details "List">}}

{{<columns>}}

- A.A.V. Newsletter [500]
- Abstracts of the Papers Communicated to the Royal Society of London [500]
- Abstracts of the Papers Printed in the Philosophical Transactions of the Royal
  Society of London [1500]
- AIBS Bulletin [1000]
- Ambio [5000]
- American Fern Journal [4500]
- American Journal of Botany [15000]
- American Scientist [21500]
- American Zoologist [3500]
- Annalen des Naturhistorischen Museums in Wien [3500]
- Annales Zoologici Fennici [2500]
- Annals of Botany [14000]
- Annals of the Missouri Botanical Garden [3500]
- Annual Review of Ecology, Evolution, and Systematics [1000]
- Applied Vegetation Science [500]
- Arctic and Alpine Research [2000]
- Arctic, Antarctic, and Alpine Research [1000]
- Avian Diseases [8000]
- Bears: Their Biology and Management [500]
- Behavioral Ecology and Sociobiology [5500]
- Behaviour [4500]
- Belgian Journal of Botany [500]
- Biodiversity Letters [100]
- Biogeochemistry [3000]
- Biological Bulletin [9000]
- Biology and Environment: Proceedings of the Royal Irish Academy [500]
- Biometrika [8000]
- Bios [5500]
- BioScience [14500]
- Biotropica [3500]
- Bird-Banding [3500]
- Botanical Gazette [13500]
- Botanical Review [1500]
- Brittonia [4000]
- Bulletin (Association for Tropical Biology) [100]
- Bulletin de la Société Royale de Botanique de Belgique / Bulletin van de
  Koninklijke Belgische Botanische Vereniging [3500]
- Bulletin du Jardin botanique de l'État a Bruxelles [1000]
- Bulletin du Jardin botanique national de Belgique / Bulletin van de National
  Plantentuin van België [1000]
- Bulletin of Miscellaneous Information (Royal Botanic Gardens, Kew) [3000]
- Bulletin of the Ecological Society of America [4500]
- Bulletin of the Northeastern Bird-Banding Association [500]
- Bulletin of the Nuttall Ornithological Club [1000]
- Bulletin of the Torrey Botanical Club [11000]
- Bulletins from the Ecological Research Committee [200]
- Caldasia [1500]
- Castanea [3500]
- Cell Stress & Chaperones [1500]
- Chesapeake Science [1000]
- Chinese Science [100]
- Colonial Waterbirds [1000]
- Conservation Biology [5500]
- Copeia [14500]
- Crustaceana [6000]
- Current Science [56000]
- Diversity and Distributions [1500]
- East Asian Science, Technology, and Medicine [500]
- Ecography [1500]
- Ecological Applications [4000]
- Ecological Bulletins [1000]
- Ecological Monographs [2000]
- Ecology [20500]
- Economic Botany [6000]
- Ecosystems [1500]
- Elementary School Science Bulletin [1000]
- Englera [100]
- Estuaries [2500]
- Estuaries and Coasts [1000]
- Evolution [11000]
- Flora Neotropica [500]
- Florida Scientist [2000]
- Folia Geobotanica [1000]
- Folia Geobotanica & Phytotaxonomica [2000]
- Freshwater Invertebrate Biology [100]
- Freshwater Science [500]
- Frontiers in Ecology and the Environment [3000]
- Functional Ecology [3500]
- Global Ecology and Biogeography [1000]
- Global Ecology and Biogeography Letters [500]
- Great Basin Naturalist Memoirs [200]
- Herpetologica [5000]
- Herpetological Monographs [200]
- History and Philosophy of the Life Sciences [2500]
- Holarctic Ecology [500]
- Human Biology [5500]
- Human Ecology Review [1000]
- Integrative and Comparative Biology [1000]
- International Journal of Plant Sciences [2500]
- Invertebrate Biology [1000]
- In Vitro [2000]
- In Vitro Cellular & Developmental Biology [1500]
- In Vitro Cellular & Developmental Biology. Animal [2500]
- In Vitro Cellular & Developmental Biology. Plant [2000]
- Irish Journal of Agricultural and Food Research [500]
- Irish Journal of Agricultural Economics and Rural Sociology [500]
- Irish Journal of Agricultural Research [1000]
- Irish Journal of Earth Sciences [500]
- Irish Journal of Food Science and Technology [500]
- Issues in Science and Technology [4000]
- Journal (American Water Works Association) [1500]
- Journal of Animal Ecology [7000]
- Journal of Applied Ecology [6000]
- Journal of Avian Biology [1500]
- Journal of Avian Medicine and Surgery [1000]
- Journal of Biogeography [5000]
- Journal of Coastal Conservation [500]
- Journal of Coastal Research [8000]
- Journal of College Science Teaching [8000]
- Journal of Crustacean Biology [3000]
- Journal of Earth Sciences [100]
- Journal of Ecology [8500]
- Journal of Elementary Science Education [500]
- Journal of Experimental Botany [12500]
- Journal of Field Ornithology [3000]
- Journal of Herpetology [4500]
- Journal of Mammalogy [12500]
- Journal of Orthoptera Research [500]
- Journal of Paleontology [10500]
- Journal of Range Management [8000]
- Journal of Science Teacher Education [500]
- Journal of the Arizona Academy of Science [500]
- Journal of the Arizona-Nevada Academy of Science [500]
- Journal of the Association of Avian Veterinarians [1000]
- Journal of the Elisha Mitchell Scientific Society [3000]
- Journal of the History of Medicine and Allied Sciences [6500]
- Journal of the Kansas Entomological Society [5500]
- Journal of the New York Entomological Society [5000]
- Journal of the North American Benthological Society [2000]
- Journal of the North Carolina Academy of Science [500]
- Journal of the Ohio Herpetological Society [100]

<--->

- Journal of the Pennsylvania Academy of Science [1000]
- Journal of the Washington Academy of Sciences [7500]
- Journal of Tropical Ecology [2500]
- Journal of Vegetation Science [3000]
- Journal of Vertebrate Paleontology [3000]
- Journal of Zoo and Wildlife Medicine [2500]
- Kew Bulletin [7500]
- Lindbergia [1000]
- Maine Naturalist [100]
- Mammalian Species [1000]
- Marine Ecology Progress Series [13500]
- Maryland Tidewater News [2000]
- Mathematical Proceedings of the Royal Irish Academy [500]
- Memoirs of the Torrey Botanical Club [200]
- Memoir (The Paleontological Society) [200]
- Microbial Ecology [3500]
- Micropaleontology [2500]
- Minerva [2000]
- Missouri Botanical Garden Annual Report [200]
- Mountain Research and Development [3000]
- Mycologia [12000]
- Mycological Bulletin [200]
- New Phytologist [1500]
- Northeastern Naturalist [1000]
- Northwestern Naturalist [1000]
- Notizblatt des Königl. botanischen Gartens und Museums zu Berlin [1500]
- Novon [2500]
- Oecologia [13000]
- Oikos [8500]
- Opflow [1500]
- Ornis Scandinavica (Scandinavian Journal of Ornithology) [1000]
- Ornithological Monographs [500]
- PALAIOS [2000]
- Paleobiology [2000]
- Palynology [500]
- Philosophical Transactions of the Royal Society of London [14000]
- Philosophical Transactions of the Royal Society of London. Series A,
  Mathematical and Physical Sciences [11500]
- Philosophical Transactions of the Royal Society of London. Series B,
  Biological Sciences [11000]
- Physiological and Biochemical Zoology: Ecological and Evolutionary Approaches
  [1500]
- Physiological Zoology [3500]
- Plant Cell [500]
- Plant Ecology [3000]
- Plant Ecology and Evolution [500]
- Plant Physiology [31500]
- Politics and the Life Sciences [1500]
- Proceedings: Biological Sciences [10000]
- Proceedings: Mathematical, Physical and Engineering Sciences [5000]
- Proceedings of the Academy of Natural Sciences of Philadelphia [7500]
- Proceedings of the American Society of Microscopists [500]
- Proceedings of the Annual Meeting. American Association of Stratigraphic
  Palynologists [100]
- Proceedings of the Florida Academy of Sciences [500]
- Proceedings of the National Academy of Sciences of the United States of
  America [124000]
- Proceedings of the Pennsylvania Academy of Science [3000]
- Proceedings of the Royal Irish Academy [1500]
- Proceedings of the Royal Irish Academy. Polite Literature and Antiquities
  [100]
- Proceedings of the Royal Irish Academy. Science [500]
- Proceedings of the Royal Irish Academy. Section A: Mathematical and Physical
  Sciences [1000]
- Proceedings of the Royal Irish Academy. Section B: Biological, Geological, and
  Chemical Science [1000]
- Proceedings of the Royal Irish Academy. Section C: Archaeology, Celtic
  Studies, History, Linguistics, Literature [1000]
- Proceedings of the Royal Society of London [5500]
- Proceedings of the Royal Society of London. Series A, Mathematical and
  Physical Sciences [15500]
- Proceedings of the Royal Society of London. Series B, Biological Sciences
  [6500]
- Proceedings of the Washington Academy of Sciences [200]
- Quarterly Journal of the Florida Academy of Sciences [1000]
- Radiation Research [13000]
- Radiation Research Supplement [500]
- Rangeland Ecology & Management [1000]
- Rangelands [4000]
- Rangeman's Journal [500]
- Science and Children [14500]
- Science Progress [16500]
- Science Scope [6000]
- Sigma Xi Quarterly [1000]
- Southeastern Naturalist [1000]
- Systematic Biology [2000]
- Systematic Botany [3000]
- Systematic Botany Monographs [500]
- Systematics and Geography of Plants [500]
- Systematic Zoology [2500]
- Taxon [12000]
- The American Biology Teacher [16500]
- The American Midland Naturalist [9000]
- The American Naturalist [20000]
- The Asa Gray Bulletin [500]
- The Auk [25500]
- The Bryologist [8500]
- The Coleopterists Bulletin [4500]
- The Condor [11000]
- The Florida Entomologist [7000]
- The Great Basin Naturalist [2500]
- The Irish Naturalist [5000]
- The Irish Naturalists' Journal [8000]
- The Journal of Arachnology [2500]
- The Journal of Mycology [1000]
- The Journal of Parasitology [18000]
- The Journal of the Torrey Botanical Society [1000]
- The Journal of Wildlife Management [12500]
- The Journal of Zoo Animal Medicine [1000]
- The Micropaleontologist [500]
- The Murrelet [3000]
- The New Phytologist [15500]
- The Plant Cell [7000]
- The Plant World [2000]
- The Quarterly Review of Biology [33000]
- The Science Teacher [21500]
- The Southwestern Naturalist [5500]
- The Wilson Bulletin [11500]
- The Wilson Journal of Ornithology [1000]
- Torreya [3500]
- Transactions of the American Entomological Society [2500]
- Transactions of the American Entomological Society and Proceedings of the
  Entomological Section of the Academy of Natural Sciences [100]
- Transactions of the American Microscopical Society [6000]
- Transactions of the Annual Meetings of the Kansas Academy of Science [500]
- Transactions of the Kansas Academy of Science [5000]
- Ursus [500]
- Vegetatio [3500]
- Waterbirds: The International Journal of Waterbird Biology [1500]
- Weeds [1500]
- Weed Science [7000]
- Weed Technology [4500]
- Western North American Naturalist [1000]
- Wildlife Monographs [500]
- Wildlife Society Bulletin [5000]
- Willdenowia [3000]
- Zeitschrift für Morphologie und Ökologie der Tiere [1500]

{{</columns>}}

{{</details>}}

## Processing

- **OCR to plain text:** Unknown, performed by JSTOR. Via our communications
  with the folks at JSTOR, we know that they OCR text using whatever they
  believe is the current "best practice" software. But they do not ever, to our
  knowledge, re-OCR their back catalog, nor do they provide us with any
  information about the OCR package used for each article.
- **Metadata:** Provided directly by JSTOR.
- **PMIDs, PMCIDs, and PubMed Manuscript IDs:** [PubMed
  scraping]({{<relref "/technical-details/pubmed-scraping.md">}})
- **Keywords and Tags:** Keywords, if present, are proper author-generated
  keywords. Tags are a variety of strange automatically generated JSTOR tags and
  categories, including, for instance, differentiating "normal" articles from
  book reviews.

## Changelog

- **Data Source Version 1 (2021-07-02):** First import of our JSTOR data into
  the new data format.
